import pytorch_lightning as pl
from torchvision import transforms
from torch.utils.data import DataLoader
from data.cityscapes_dataset import CityscapesSegmentation
import argparse

root = 'Cityscapes/'
BATCH_SIZE = 4


class CITYSCAPES(pl.LightningDataModule):
    def __init__(self, args: argparse.Namespace = None):
        self.args = vars(args) if args is not None else {}
        self.batch_size = self.args.get("batch_size", BATCH_SIZE)
        self.transform = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize(
                    mean=[0.35675976, 0.37380189, 0.3764753], std=[0.32064945, 0.32098866, 0.32325324]
                ),
            ]
        )

    def setup(self):
        self.train_dataset = CityscapesSegmentation(root, transform=self.transform)
        self.val_dataset = CityscapesSegmentation(root, split='val', transform=self.transform)
        self.test_dataset = CityscapesSegmentation(root, split='test', transform=self.transform)

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.batch_size, shuffle=False)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False)

    @staticmethod
    def add_to_argparse(parser):
        parser.add_argument("--batch_size", type=int, default=BATCH_SIZE)
