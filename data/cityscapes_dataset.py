import os
import numpy as np
from PIL import Image
import torch
from torch.utils.data import Dataset


class CityscapesSegmentation(Dataset):

    def __init__(self, root, split='train', img_size=(2048, 1024), mode='fine', transform=None):
        self.root = os.path.expanduser(root)
        self.mode = 'gtFine' if mode == 'fine' else 'gtCoarse'
        self.images_dir = os.path.join(self.root, 'leftImg8bit', split)
        self.targets_dir = os.path.join(self.root, self.mode, split)
        self.transform = transform
        self.split = split
        self.img_size = img_size
        self.images = []
        self.targets = []

        self.void_labels = [0, 1, 2, 3, 4, 5, 6, 9, 10, 14, 15, 16, 18, 29, 30, -1]
        self.valid_labels = [7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33]
        self.valid_labels_color = [
            (128, 64, 128),  # road
            (244, 35, 232),  # sidewalk
            (70, 70, 70),  # building
            (102, 102, 156),  # wall
            (190, 153, 153),  # fence
            (153, 153, 153),  # pole
            (250, 170, 30),  # traffic light
            (220, 220, 0),  # traffic sign
            (107, 142, 35),  # vegetation
            (152, 251, 152),  # terrain
            (70, 130, 180),  # sky
            (220, 20, 60),  # person
            (255, 0, 0),  # rider
            (0, 0, 142),  # car
            (0, 0, 70),  # truck
            (0, 60, 100),  # bus
            (0, 80, 100),  # train
            (0, 0, 230),  # motorcycle
            (119, 11, 32),  # bicycle
            (0, 0, 142)  # license plate
        ]
        self.class_map = dict(zip(self.valid_labels, range(len(self.valid_labels))))
        self.mask2rgb_map = dict(zip(self.valid_labels, self.valid_labels_color))
        self.class2rgb_map = dict(zip(range(len(self.valid_labels)), self.valid_labels_color))
        self.ignore_index = 255

        if mode not in ['fine', 'coarse']:
            raise ValueError('Invalid mode! Please use mode="fine" or mode="coarse"')

        if mode == 'fine' and split not in ['train', 'test', 'val']:
            raise ValueError('Invalid split for mode "fine"! Please use split="train", split="test"'
                             ' or split="val"')
        elif mode == 'coarse' and split not in ['train', 'train_extra', 'val']:
            raise ValueError('Invalid split for mode "coarse"! Please use split="train", split="train_extra"'
                             ' or split="val"')
        if mode not in ['fine', 'coarse']:
            raise ValueError('Invalid mode! Please use mode="fine" or mode="coarse"')
        if mode == 'fine' and split not in ['train', 'test', 'val']:
            raise ValueError('Invalid split for mode "fine"! Please use split="train", split="test" or split="val"')
        elif mode == 'coarse' and split not in ['train', 'train_extra', 'val']:
            raise ValueError(
                'Invalid split for mode "coarse"! Please use split="train", split="train_extra" or split="val"')
        if not os.path.isdir(self.images_dir) or not os.path.isdir(self.targets_dir):
            raise RuntimeError('Dataset not found or incomplete. Please make sure all required folders for the'
                               ' specified "split" and "mode" are inside the "root" directory')

        for city in os.listdir(self.images_dir):
            img_dir = os.path.join(self.images_dir, city)
            target_dir = os.path.join(self.targets_dir, city)
            for file_name in os.listdir(img_dir):
                self.images.append(os.path.join(img_dir, file_name))
                target_name = '{}_{}'.format(file_name.split('_leftImg8bit')[0], '{}_labelIds.png'.format(self.mode))
                self.targets.append(os.path.join(target_dir, target_name))

    def __getitem__(self, index):
        img = Image.open(self.images[index]).convert('RGB')
        img = img.resize(self.img_size)
        img = np.array(img)

        target = Image.open(self.targets[index])
        target = target.resize(self.img_size)
        target = np.array(target)
        # targetrgb = self.mask2rgb(target)
        target = self.mask2class(target)

        if self.transform:
            img = self.transform(img)

        return img, target

    def mask2class(self, mask):
        for voidc in self.void_labels:
            mask[mask == voidc] = self.ignore_index
        for validc in self.valid_labels:
            mask[mask == validc] = self.class_map[validc]
        return mask

    def class2rgb(self, mask):
        rgbimg = torch.zeros((3, mask.size()[0], mask.size()[1]), dtype=torch.uint8)
        for c in self.class_map:
            rgbimg[0][mask == c] = self.class2rgb_map[c][0]
            rgbimg[1][mask == c] = self.class2rgb_map[c][1]
            rgbimg[2][mask == c] = self.class2rgb_map[c][2]
        return rgbimg

    def mask2rgb(self, mask):
        rgbimg = torch.zeros((3, mask.shape[0], mask.shape[1]), dtype=torch.uint8)
        for validc in self.valid_labels:
            rgbimg[0][mask == validc] = self.mask2rgb_map[validc][0]
            rgbimg[1][mask == validc] = self.mask2rgb_map[validc][1]
            rgbimg[2][mask == validc] = self.mask2rgb_map[validc][2]
        return rgbimg
