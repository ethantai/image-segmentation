import pytorch_lightning as pl
import torch.nn.functional as F
import argparse

LR = 1e-3


class BaseLitModel(pl.LightningModule):

    def __init__(self, model, args: argparse.Namespace = None):
        super().__init__()
        self.args = vars(args) if args is not None else {}

        self.model = model
        self.lr = self.args.get("lr", LR)

    def configure_optimizers(self):
        optimizer = self.optimizer_class(self.parameters(), lr=self.lr)
        return {"optimizer": optimizer, "monitor": "val_loss"}

    def forward(self, x):
        output = self.model(x)
        return output

    def training_step(self, batch, batch_idx):
        img, mask = batch
        img = img.float()
        mask = mask.long()
        out = self(img)
        loss = F.cross_entropy(out, mask, ignore_index=255)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        img, mask = batch
        img = img.float()
        mask = mask.long()
        out = self(img)
        loss = F.cross_entropy(out, mask, ignore_index=255)
        self.log("val_loss", loss, prog_bar=True)
    '''   
    def test_step(self, batch, batch_idx):
        img, mask = batch
        img = img.float()
        mask = mask.long()
        out = self(img)
        self.log("val_loss", loss, prog_bar=True)
    '''

    @staticmethod
    def add_to_argparse(parser):
        parser.add_argument("--lr", type=float, default=LR)
