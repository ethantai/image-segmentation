import argparse
import importlib

import numpy as np
import torch
import pytorch_lightning as pl
import wandb

import models

np.random.seed(42)
torch.manual_seed(42)


def _import_class(module_and_class_name: str) -> type:
    """Import class from a module, e.g. 'models.UNet' """
    module_name, class_name = module_and_class_name.rsplit(".", 1)
    module = importlib.import_module(module_name)
    class_ = getattr(module, class_name)
    return class_


def _setup_parser():
    """Set up Python's ArgumentParser with data, models, and other arguments."""
    parser = argparse.ArgumentParser(add_help=False)

    trainer_parser = pl.Trainer.add_argparse_args(parser)
    trainer_parser._action_groups[1].title = "Trainer Args"
    parser = argparse.ArgumentParser(add_help=False, parents=[trainer_parser])

    # Basic arguments
    parser.add_argument("--wandb", action="store_true", default=False)
    parser.add_argument("--model_class", type=str, default="UNet")
    parser.add_argument("--data_class", type=str, default="CITYSCAPES")

    # Get the models classes, so that we can add their specific arguments
    temp_args, _ = parser.parse_known_args()
    model_class = _import_class(f"models.{temp_args.model_class}")
    data_class = _import_class(f"data.{temp_args.data_class}")

    # Get models, data, and LitModel specific arguments
    model_group = parser.add_argument_group("Model Args")
    model_class.add_to_argparse(model_group)

    data_group = parser.add_argument_group("Data Args")
    data_class.add_to_argparse(data_group)

    lit_model_group = parser.add_argument_group("LitModel Args")
    models.BaseLitModel.add_to_argparse(lit_model_group)

    parser.add_argument("--help", "--h", action="help")
    return parser


def main():

    parser = _setup_parser()
    args = parser.parse_args()

    data_class = _import_class(f"data.{args.data_class}")
    data = data_class(args)

    model_class = _import_class(f"models.{args.model_class}")
    model = model_class(args=args)
    lit_model_class = models.BaseLitModel
    lit_model = lit_model_class(args=args, model=model)

    logger = pl.loggers.TensorBoardLogger("training/logs/")

    if args.wandb:
        logger = pl.loggers.WandbLogger()
        logger.watch(model)
        logger.log_hyperparams(vars(args))

    early_stopping_callback = pl.callbacks.EarlyStopping(monitor="val_loss", mode="max", patience=10)
    model_checkpoint_callback = pl.callbacks.ModelCheckpoint(
        filename="{epoch:03d}-{val_loss:.3f}", monitor="val_loss", mode="max"
    )
    callbacks = [early_stopping_callback, model_checkpoint_callback]
    args.weights_summary = "full"
    trainer = pl.Trainer.from_argparse_args(args, callbacks=callbacks, logger=logger, weights_save_path="training/logs")

    trainer.tune(lit_model, datamodule=data)
    trainer.fit(lit_model, datamodule=data)
    trainer.test(lit_model, datamodule=data)

    best_model_path = model_checkpoint_callback.best_model_path
    if best_model_path:
        print("Best model saved at:", best_model_path)
        if args.wandb:
            wandb.save(best_model_path)
            print("Best model also uploaded to W&B")


if __name__ == "__main__":
    main()
